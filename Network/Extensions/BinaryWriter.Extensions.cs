﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts.Network.Extensions
{
	public static class BinaryWriterExtensions
	{
		public static void Write(this BinaryWriter bw, Color color)
		{
			byte[] rgb = new byte[3];

			rgb[0] = (byte)color.r;
			rgb[1] = (byte)color.g;
			rgb[2] = (byte)color.b;

			bw.Write(rgb, 0, 3);
		}
	}
}

