using System.IO;

using Assets.Scripts.Network.Extensions;

namespace Assets.Scripts.Network.Packets
{
    /// <summary>
    /// The PlayerStatus (0x4) packet.
    /// </summary>
    public class PlayerStatus : InfinityPacket
    {

        public PlayerStatusTypes StatusId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerStatus"/> class.
        /// </summary>
        public PlayerStatus()
            : base((byte)PacketTypes.PlayerStatus)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerStatus"/> class.
        /// </summary>
        /// <param name="statusId">Current state of the client.</param>
        public PlayerStatus(PlayerStatusTypes statusId)
            : base((byte)PacketTypes.PlayerStatus)
        {
            this.StatusId = statusId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerStatus"/> class.
        /// </summary>
        /// <param name="br">br</param>
        public PlayerStatus(BinaryReader br)
            : base(br)
        {
            this.StatusId = (PlayerStatusTypes)br.ReadByte();
        }

        public override string ToString()
        {
            return $"[PlayerStatus: StatusId={ StatusId }]";
        }

        #region implemented abstract members of InfinityPacket

        public override short GetLength()
        {
            return (short) 1;
        }

        public override void ToStream(Stream stream, bool includeHeader = true)
        {
            /*
             * Length and ID headers get written in the base packet class.
             */
            if (includeHeader)
            {
                base.ToStream(stream, includeHeader);
            }

            /*
             * Always make sure to not close the stream when serializing.
             * 
             * It is up to the caller to decide if the underlying stream
             * gets closed.  If this is a network stream we do not want
             * the regressions of unconditionally closing the TCP socket
             * once the payload of data has been sent to the client.
             */
            using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, new System.Text.UTF8Encoding()))
            {
                br.Write((byte)StatusId);
            }
        }

        #endregion

    }
}
