using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Network;

namespace Assets.Scripts.Utils {
    public class NetworkUtils : MonoBehaviour
    {
#if INFINITY_CLIENT
        private static string NetworkObjectName = "Network Client";
        private static string NetworkScriptName = "NetClient";
        public static bool IsNetworkClient = true;
#else
        private static string NetworkObjectName = "Network Server";
        private static string NetworkScriptName = "NetServer";
        public static bool IsNetworkClient = false;
#endif

        public static NetBase FindNetworkScript()
        {
            var netobject = GameObject.Find("Network Server");

            if (netobject == null)
            {
                throw new MissingComponentException($"No { NetworkObjectName } object could be found in the scene.");
            }

#if INFINITY_CLIENT
            var netscript = netobject.GetComponent<NetClient>();
#else
            var netscript = netobject.GetComponent<NetServer>();
#endif
            if (netscript == null)
            {
                throw new MissingComponentException($"No { NetworkScriptName } component found attached to the { NetworkObjectName }");
            }

            return netscript;
        }

        public static DataHandler FindDataHandler()
        {
            var netscript = FindNetworkScript();
            return netscript.DataHandler;
        }

        public static ConnectionHandler FindConnectionHandler()
        {
            var netscript = FindNetworkScript();
            return netscript.ConnectionHandler;
        }
    }
}
