using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Assets.Scripts.Network.Events;
using Assets.Scripts.Network.Packets;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Network
{
#if INFINITY_CLIENT
    class NetClient : NetBase
    {

        private string IPAddress = "127.0.0.1";
        private string Port = "6606";
        private string Username = "Player";

        public enum ConnectionState : byte
        {
            Disconnected = 1,
            Connecting,
            Connected
        }
        public ConnectionState State;

        private AsyncOperation asyncLoadLevel;

        private PanelManager PanelManager;
        public Animator ErrorAnimator;
        public Animator ConnectingAnimator;

        void Start()
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.LoadScene("Scenes/Menu", LoadSceneMode.Single);
            ConnectionHandler = new ClientConnectionHandler(); // Handles raw sockets
            DataHandler = new DataHandler(); // Handles the packet data and fires events
            PacketHandler = new PacketHandler(DataHandler); // Handles the raw byte stream and parses into readable data

            ConnectionHandler.InitNetwork();

            DataHandler.LoadLevel.AddListener(OnLoadLevel);
        }

        void Update()
        {
            NetworkReceive();
        }

        protected override void OnConnect(int hostId, int connectionId, NetworkError error)
        {
            if (connectionId == ConnectionHandler.ConnectionId)
            {
                Debug.Log("Connection established");
                State = ConnectionState.Connected;
                ConnectionHandler.SendData(new ConnectRequest(Username), NetworkChannel.AllCostDelivery, ConnectionHandler.ConnectionId);
            }
        }

        protected override void OnDisconnect(int hostId, int connectionId, NetworkError error)
        {
            if (connectionId != ConnectionHandler.ConnectionId)
            {
                return;
            }

            if (State == ConnectionState.Connecting)
            {
                State = ConnectionState.Disconnected;
                LoadMenuWithError("Failed to connect to server", error.ToString());
                return;
            }
            else if (State == ConnectionState.Connected)
            {
                State = ConnectionState.Disconnected;
                LoadMenuWithError("Lost connection to server", error.ToString());
                DataHandler.HandleDisconnect(connectionId, error);
                return;
            }

            Debug.Log("OnDisconnect(hostId = " + hostId + ", connectionId = "
                + connectionId + ", error = " + error.ToString() + ")");
        }

        protected override void OnBroadcast(int hostId, byte[] data, int size, NetworkError error)
        {
            Debug.Log("OnBroadcast(hostId = " + hostId + ", data = "
                + data + ", size = " + size + ", error = " + error.ToString() + ")");
        }

        protected override void OnData(int hostId, int connectionId, int channelId, byte[] data, int size, NetworkError error)
        {
            if (State != ConnectionState.Connected)
            {
                return;
            }

            PacketHandler.HandlePacket(data, connectionId);
        }

        public void Connect(string IpAddress, int Port, string Username)
        {
            var MenuManager = GameObject.Find("MenuManager");
            PanelManager = MenuManager.GetComponent<PanelManager>();

            State = ConnectionState.Connecting;
            PanelManager.OpenConnectingPanel();

            (ConnectionHandler as ClientConnectionHandler).Connect(IPAddress, Port);
        }

        public void Disconnect()
        {
            State = ConnectionState.Disconnected;
            ConnectionHandler.SendData(new Disconnect("Disconnected by user"), NetworkChannel.AllCostDelivery);
        }

        void LoadMenuWithError(string brief, string error)
        {
            StartCoroutine(LoadMenuWithErrorAsync(brief, error));
        }

        IEnumerator LoadMenuWithErrorAsync(string brief, string error)
        {
            asyncLoadLevel = SceneManager.LoadSceneAsync("Scenes/Menu", LoadSceneMode.Single);
            while (!asyncLoadLevel.isDone)
            {
                yield return null;
            }

            var MenuManager = GameObject.Find("MenuManager");
            var PanelManager = MenuManager.GetComponent<PanelManager>();

            PanelManager.OpenErrorPanel(brief, error.ToString());
        }

        void OnLoadLevel(PacketEventArgs args)
        {
            Debug.Log("Loading level");
            SceneManager.LoadScene("Scenes/Lobby", LoadSceneMode.Single);
        }

    }
#endif
}
