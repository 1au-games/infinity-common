namespace Assets.Scripts.Network
{
	public enum PacketTypes : byte
	{
		/*001*/ ConnectRequest = 1,
		/*002*/ Disconnect,
        /*003*/ LoadLevel,
        /*004*/ PlayerStatus,
    }
}

