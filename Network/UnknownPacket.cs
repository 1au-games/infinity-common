﻿using System;
using System.IO;

using Assets.Scripts.Network.Extensions;

namespace Assets.Scripts.Network
{
	public class UnknownPacket : InfinityPacket
	{
		public byte[] payload { get; set; }

		public UnknownPacket(BinaryReader br)
			: base(br)
		{
			payload = br.ReadBytes(_length - InfinityPacket.PACKET_HEADER_LEN);
		}

        public override short GetLength()
		{
			return (short)(_length - InfinityPacket.PACKET_HEADER_LEN);
		}

		public override void ToStream(Stream stream, bool includeHeader = true)
		{
			base.ToStream(stream, includeHeader);

			using (BinaryWriterLeaveOpen bw = new BinaryWriterLeaveOpen(stream, System.Text.Encoding.UTF8)) {
				bw.Write(payload);
			}
		}

		public override string ToString()
		{
			string hex = BitConverter.ToString(payload).Replace("-", string.Empty);

			return string.Format("[UnknownPacket: ID={0} Len={1} Content={2}]", ID, GetLength(), hex);

        }
	}
}

